Object Hall "Hall"
with
    description [;
        print "Vous êtes dans le hall d'entrée d'un ancien manoir.";
        list_exits();
        print "Vous voyez un portemanteau, un miroir";
        if (miroir has general)
            print " brisé";
        " et une porte", (o_state) porte_entree, ".";
    ],
    before [;
        Examine:
            if (noun == player && miroir hasnt general)
                "Le miroir vous regarde droit dans les yeux.";
        Sing:
            if (miroir hasnt general) {
                print (string) MSG_SING_DEFAULT;
                "^^Le miroir en frémit de contentement.";
        }
    ],
    n_to Couloir,
    s_to porte_entree
has light;

Object Couloir "Couloir"
with
    description [;
        print "Vous êtes dans un long couloir sombre et étroit.";
        list_exits();
        print "Vous voyez un tableau ";
        if (tableau has general)
            print "découpé";
        else
            print "sur toile";
        if (niche in self)
            print ", une niche architecturale";
        if (grille has light)
            ", une moquette usagée et une grille de ventilation au sol.";
        " et une moquette recouvrant le sol.";
    ],
    n_to Cage_escalier,
    s_to Hall,
    e_to Salon,
    w_to Cuisine,
has light;

Object Salon "Salon"
with
    description [;
        print "Vous êtes dans un salon illuminé d'un feu de cheminée.";
        list_exits();
        print "Vous voyez une pendule et un fauteuil club";
        if (fauteuil has general)
            print " (déplacé)";
        ".";
    ],
    after [;
        Wait:
            if (cierge has on) {
                give cierge ~on;
                remove cierge;
                give vitrail ~concealed;
                remove cercueil;
                move cire to Chapelle;
                "Le cierge de la chapelle ardente se consume rapidement sans
                aucune trace apparente de brûlures.";
            }
    ],
    w_to Couloir,
has light;

Object Cuisine "Cuisine"
with
    description [;
        print "Vous êtes dans une cuisine immaculée sans feu ni eau courante.";
        list_exits();
        print "Vous voyez un placard", (o_state) placard, " et une porte";
        if (porte_cellier has open && chaine hasnt open)
            print " (entrouverte)";
        else
            print (o_state) porte_cellier;
        ".";
    ],
    s_to [;
        if (porte_cellier has open && chaine hasnt open)
            "C'est impossible, car une chaîne de sécurité bloque l'ouverture
            de la porte.";
        return porte_cellier;
    ],
    e_to Couloir,
has light;

Object Cellier "Cellier"
with
    description [;
        print "Vous êtes dans un cellier mal éclairé sur un sol en terre
        battue.";
        list_exits();
        print "Vous voyez ";
        if (eau in self)
            "de l'eau qui s'égoutte du";
        print "des taches d'humidité au";
        print " plafond et une porte";
        if (porte_cellier has open && chaine hasnt open)
            print " (entrouverte)";
        else
            print (o_state) porte_cellier;
        ".";
    ],
    n_to porte_cellier,
has light;

Object Cage_escalier "Cage d'escalier"
with
    description [;
        print "Vous êtes dans une cage d'escalier ouverte et lumineuse.";
        list_exits();
        "Vous voyez un lustre d'apparat et une rampe d'escalier.";
    ],
    s_to Couloir,
    e_to Bibliotheque,
    w_to Chambre,
    u_to Palier,
    d_to Cave,
has light;

Object Bibliotheque "Bibliothèque"
with
    description [;
        print "Vous êtes dans une bibliothèque garnie de livres du sol
        au plafond.";
        list_exits();
        "Vous voyez un buste en marbre et un globe terrestre.";
    ],
    w_to Cage_escalier,
has light;

Object Chambre "Chambre"
with
    description [;
        print "Vous êtes dans une chambre d'enfant non genrée.";
        list_exits();
        print "Vous voyez un lit double, une armoire", (o_state) armoire, " et
        un coffre à jouets", (o_state) coffre;
        if (self has general) {
            give self ~general;
            print "^^Vous sortez du lit sur-le-champ";
        }
        ".";
    ],
    e_to Cage_escalier,
has light ~general;

Object Cave "Cave"
with
    description [;
        print "Vous êtes dans une petite cave voûtée.";
        list_exits();
        "Vous voyez un tonneau à vin", (o_state) tonneau, " et une grille de
        ventilation au plafond.";
    ],
    u_to Cage_escalier,
has ~light;

Object Palier "Palier"
with
    description [;
        print "Vous êtes sur le palier du dernier étage du manoir.";
        list_exits();
        print "Vous voyez une fenêtre à lancette et une plante ";
        if (plante has general)
            print "artificielle";
        else
            print "verte";
        ".";
    ],
    w_to Chapelle,
    d_to Cage_escalier,
has light;

Object Chapelle "Chapelle" !TODO +autel
with
    description [;
        print "Vous êtes dans une chapelle ardente.";
        list_exits();
        print "Vous voyez un vitrail";
        if (vitrail has general)
                print " cassé";
        if (cierge in self) {
            if (vitrail has general || cierge has on)
                print ", ";
            else
                print " et ";
            print "un cierge", (o_state) cierge;
        }
        if (vitrail has general || cierge has on)
            print " et un cercueil", (o_state) cercueil;
        ".";
    ],
    e_to Palier,
has light;
