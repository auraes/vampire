Object bague "bague"
with
    description "Une bague fantôme unisexe de style gothique.",
    short_name [;
        if (self in player) {
            print "bague fantôme";
            rtrue;
        }
    ],
    name 'bague',
    article ARTICLE_UNE,
    after [;
        Take, Remove:
            if (self hasnt general) {
                give self general;
                if (child(tiroir_1))
                    move (child(tiroir_1)) to coffre;
                if (child(tiroir_3))
                    move (child(tiroir_3)) to coffre;
                give tiroir_2 ~open locked;
                "Vous prenez la bague avec vous et refermez précipitamment le
                tiroir.";
            }
        wear:
            "Vous passez la bague à votre index droit donnant à votre main une
            carnation légère et diaphane.";
        disrobe:
            "Votre retirez la bague rendant à votre main sa carnation
            naturelle.";
    ],
has female clothing ~worn ~general;

Object baton "bâton"
with
    description "Un morceau de bois contondant sauvé in extremis des flammes.",
    name 'bâton',
    article ARTICLE_UN,
has;

Object brandon "brandon"
with
    short_name [;
        if (self in player) {
            print "torche (enflammée)";
            rtrue;
        }
    ],
    description "Un brandon de papier torsadé enflammé.",
    name 'torche' 'brandon' 'papier',
    article ARTICLE_UN,
    before [;
        Drop, SwitchOff, Blow:
            self.time_left = 0;
            give self general;
            rtrue;
    ],
    time_out [;
        remove self;
        if (self hasnt general)
            new_line;
        give self ~general;
        "La torche de papier tombe en cendres éparses.";
    ],
    time_left 0,
has transparent ~general;

Object fake_light brandon
has light;

Object cire "cire"
with
    description "Un résidu de cire naturelle idéal en protection auditive.",
    name 'cire' 'bouchons',
    article ARTICLE_DE_LA,
    after [;
        Wear:
            if (self hasnt general) {
                give self general;
                "Vous malaxez la cire fondue entre vos doigts et la glissez
                dans vos oreilles.";
            }
            "Vous mettez les bouchons de cire dans vos oreilles.";
        Remove:
            "Vous retirez les bouchons de cire de vos oreilles.";
    ],
has female clothing ~general;

Object cle "clé"
with
    description "Une clé de meuble à anneau en trèfle.",
    name 'clé' 'clef',
    article ARTICLE_UNE,
    before [;
        Take, Remove:
            if (placard hasnt locked)
                "La clé est bloquée dans la serrure du placard et elle y
                restera.";
    ],
has female;

Object eau "eau"
with
    description [;
        if (self in cellier)
            "De l'eau de pluie due à une infiltration de la toiture.";
        "Une eau douce sans calcaire et non potable.";
    ],
    name 'eau',
    article ARTICLE_DE_L,
    before [; !TODO
        Drink:
        Take, Remove:
    ],
has female edible;

Object eclat "débris"
with
    short_name [;
        if (self in player) {
            print "fragment de miroir";
            rtrue;
        }
    ],
    description "Un éclat de miroir bien affilé.",
    name  'fragment' 'débris' 'éclat',
    article ARTICLE_UN,
has;

Object grille "grille"
with
    description "Une petite grille de ventilation pour donner un peu d'air
    frais et de lumière en sous-sol.",
    name 'grille' 'petite',
    article ARTICLE_UNE,
    found_in [;
        if (location == Cave)
            rtrue;
        if (location == Couloir && self has light)
            rtrue;
        rfalse;
    ],
has female scenery ~light;

Object livret "livret"
with
    short_name [;
        if (self in player) {
            print "livret d'opéra";
            rtrue;
        }
    ],
    name 'livret' 'livre' 'opéra',
    article ARTICLE_UN,
    react_before [;
        Sing:
            if (self in player) {
                print "« Ah ! je ris de me voir si belle en ce miroir... »^";
                if (location == HAll && miroir hasnt general) {
                    give miroir general;
                    move eclat to Hall;
                    "^À ces mots, le miroir ne se sent pas de joie et vole en
                    éclats.^^Un fragment de miroir tombe à vos pieds."; !éclat ?
                }
                rtrue;
            }
    ],
    before [;
        Examine:
            if (verb_word == 'lire') {
                if (ImplicitGrabIfNotHeld(livret))
                    rtrue;
                "C'est le fameux Air des bijoux !";
            }
            "Un livret hautement inflammable, de l'opéra Faust de Charles
            Gounod.";
        Burn, SwitchOn:
            if (action == ##SwitchOn && verb_word ~= 'allumer')
                rfalse;
            if (location == Salon) {
                if (ImplicitGrabIfNotHeld(livret))
                    rtrue;
                if (brandon in player)
                    "Vous avez déjà un brandon enflammé avec vous.";
                move_to_player(brandon);
                StartTimer(brandon, 5);
                "Vous arrachez une page du livret et l'enflammez au feu de la
                cheminée.";
            }
    ],
has;

Object maillet "maillet"
with
    description "Un maillet en bois doté d'une bonne puissance de frappe.",
    name 'maillet',
    article ARTICLE_UN,
has;

Object niche "niche"
with
    description "Une niche de style gothique abritant une statuette en bois.",
    name 'niche',
    article ARTICLE_UNE,
    capacity 1
has female scenery container open;

Object pieu "pieu affûté"
with
    description "L'arme la plus couramment utilisée pour tuer un vampire.",
    name 'pieu',
    article ARTICLE_UN,
has;

Object statuette "statuette" niche
with
    description [;
        print "Un saint Christophe s'appuyant sur un bâton";
        if (antivol in tricycle)
            print ", autour duquel est gravé le code à trois chiffres ",
            antivol.capacity;
        ".";
    ],
    name 'statue' 'statuette' 'saint' 'Christophe' 'bâton',
    article ARTICLE_UNE,
    before [;
        Take, Remove, Push, Pull, Turn:
            "C'est impossible, car la statuette est fixée à son socle.";
    ],
has concealed female;

Object vin "vin"
with
    description "TODO",
    name 'vin',
    article ARTICLE_DU,
    before [;
        Drink, Take, Remove: !TODO calice
    ],
has edible;

Object peignoir "peignoir" Hall
with
    description "Un peignoir sexy en satin de soie et dentelle.",
    name 'peignoir',
    article ARTICLE_UN,
    before [;
        Disrobe, Drop, Give:
            "Vous seriez nue et sans poches.";
        Take, Remove:
            if (self in armoire)
                "Vous préférez laisser le peignoir remisé au fond de
                l'armoire.";
    ],
has clothing worn transparent;

Object portemanteau "portemanteau" Hall
with
    description "Un ready made en bois flotté dédié aux accoutrements des
    invité(e)s.",
    name 'portemanteau',
    article ARTICLE_UN,
has scenery;

Object miroir "miroir" Hall
with
    description [;
        if (self has general)
            "TODO";
        "Un miroir sans tain, émotif et flatteur.";
    ],
    name 'miroir',
    article ARTICLE_UN,
    before [;
        Attack:
            "La violence commence là ou la parole s'arrête, grommelle le
            miroir.";
        Thrownat:
            move noun to coffre;
            give coffre ~open;
            print "Le miroir engloutit ", (the) noun, " et ";
            if (noun has pluralname)
                print "leurs";
            else
                print "son";
            " reflet", (_s) noun, " inversé", (_es) noun, ".";
        TalkTo:
            "Il n'aime que le ramage des hôtes de son toit.";
    ],
has neuter scenery ~general talkable;

Object porte_entree "porte" Hall
with
    description "Une porte d'entrée orientée sud sans poignée ni serrure.",
    name 'porte' 'entrée',
    article ARTICLE_UNE,
    door_to [;
        return Hall; !Fake
    ],
    door_dir [;
        return s_to;
    ],
has female scenery door openable ~open locked;

Object tableau "tableau" Couloir
with
    description [;
        if (self hasnt general)
            "Un portrait en pied de jumelles se tenant par la main.";
        "Un tableau sans sujet, à restaurer et sans aucune valeur monétaire.";
    ],
    name 'tableau' 'toile' 'portrait',
    article ARTICLE_UN,
    before [;
        Cut:
            if (self hasnt general && second == eclat) {
                give self general;
                remove eclat;
                move niche to Couloir;
                "Vous découpez avec soin les fibres de la toile en suivant les
                contours des sœurs jumelles, révélant une niche architecturale.
                ^^Les deux sœurs se saisissent du fragment de miroir et se
                précipitent hors du manoir, refermant avec fracas la porte
                derrière elles.";
            }
        Pull, Attack:
            "Essayez plutôt de retirer la toile de son châssis en la découpant
            avec un outil adapté.";
    ],
has scenery ~general transparent; !talkable ?

Object jumelles "jumelles" tableau !TODO
with
    description "Deux sœurs en robes bleues à la fois vivantes et mortes et
    incapables de choisir entre les deux états.",
    name 'jumelles',
    article ARTICLE_DES,
    Life [ w;
        Show:
        TalkTo:
            !if (verb_word == 'interroger')
            !   "";
        Ask, Tell: !ShoutAt
            wn = consult_from;
            while (consult_words) {
                w = NextWord();
                switch (w) {
                    'TODO':
                }
                consult_words--;
            }
            "Les jumelles ne répondent pas. Pourquoi ne pas les interroger sur
            un autre sujet ?";
        Attack, Kiss:
            "TODO";
    ],
has female pluralname concealed animate;

Object moquette "moquette" Couloir
with
    description "Un revêtement de sol en laine organique résistant à l'humidité
    et aux rayures.",
    name 'moquette' 'tapis',
    article ARTICLE_UNE,
    before [;
        Take, Remove, Pull, Attack:
            "L'idéal serait un décapeur thermique mais le jeu en est dépourvu.";
        !Burn:
    ],
has female scenery ~general;

Object tricycle "tricycle" Couloir
with
    description [;
        print "Un vélocipède facile à CONDUIRE pour des virages serrés à vive
        allure";
        if (antivol in self)
            print ".^^Il est protégé par un antivol nouvelle génération";
        ".";
    ],
    name 'tricycle' 'vélo',
    article ARTICLE_UN,
    before [;
        Enter:
            if (antivol in self)
                "C'est impossible, car le tricycle est entravé par un antivol.";
            print "Vous faites un aller-retour à toute vitesse en regardant bien
            droit devant vous.^";
            if (self.capacity) {
                --self.capacity;
                print "^Des fibres du tapis se déchirent";
                if (self.capacity == 2)
                    " libérant les bactéries qui y prolifèrent.";
                if (self.capacity == 1)
                    " à nouveau sur votre passage.";
                give grille light;
                MoveFloatingObjects();
                ", révélant une grille de ventilation.";
            }
            rtrue;
    ],
    capacity 3
has static enterable transparent;

Object antivol "antivol" tricycle
with
    description "Un antivol de cadre avec un système d'ouverture à interface
    cerveau-machine.",
    name 'antivol' 'cadenas',
    article ARTICLE_UN_,
    before [ n;
        Open: !Think
            print "En trois chiffres, à quel code pensez-vous ? ";
            n = TypeNum();
            print "^Veuillez patienter...^";
            if (n) {
                remove self;
                "^Autodestruction validée. Le système est déverrouillé.";
            }
            else {
                antivol.capacity = RandomCode();
                "^Échec de l'identification. Par mesure de sécurité le code
                a changé.";
            }
    ],
    capacity 0
has concealed lockable locked;

Object buste "buste en marbre" Bibliotheque
with
    description "Un buste sur piédestal à deux visages opposés : Éros et
    Thanatos.", !FIXME
    name 'buste',
    article ARTICLE_UN,
has scenery;

Object globe "globe terrestre" Bibliotheque
with
    description "Un globe terrestre rotatif avec une carte en relief.",
    name 'globe',
    article ARTICLE_UN,
has scenery;

Object crucifix "crucifix" Bibliotheque
with
    description "Un crucifix d'argent monté en pendentif.",
    name 'crucifix',
    article ARTICLE_UN,
has concealed clothing ~worn;

Object placard "placard" Cuisine
with
    description "Un placard de cuisine où stocker les aliments et les secrets
    honteux.",
    name 'placard',
    article ARTICLE_UN,
    after [;
        Unlock:
            move cle to Cuisine;
            "Vous déverrouillez le placard avec la clé que l'on vous a
            confiée.";
    ],
    with_key cle,
    capacity 3
has scenery container openable ~open lockable locked;

Object gousse "gousse d'ail" placard
with
    description "Une gousse d'ail bénite qui indispose les vampires.",
    name 'gousse' 'ail',
    article ARTICLE_UNE,
has female edible;

Object pilule "pilule bleue" placard
with
    description "Un comprimé de 100 mg aux effets secondaires addictifs.",
    name 'pilule',
    article ARTICLE_UNE,
has female edible;

Object porte_cellier "porte"
with
    description [;
        print "Une porte orientée ";
        if (player in Cuisine)
            print "sud";
        else
            print "nord";
        if (barre hasnt moved)
            print " ayant subi une tentative d'effraction";
        else {
            print " menant ";
            if (player in cuisine)
                print "au cellier";
            else
                print "à la cuisine";
        }
        ".";
    ],
    name 'porte',
    article ARTICLE_UNE,
    before [;
        Open:
            if (self has open && chaine hasnt open)
                "La porte est déjà entrouverte.";
        Unlock, Lock:
            "TODO";
    ],
    after [;
        Open:
            if (chaine hasnt open)
                "Vous entrouvrez la porte.";
        Close:
            if (barre has concealed) {
                give barre ~concealed;
                "Vous refermez la porte, révélant une barre métallique projetée
                à vos pieds.";
            }
    ],
    door_to [;
        if (self in Cuisine)
            return Cellier;
        return Cuisine;
    ],
    door_dir [;
        if (self in Cuisine)
            return s_to;
        return n_to;
    ],
    found_in Cuisine Cellier
has female scenery door openable open transparent;

Object chaine "chaîne" porte_cellier !chaînette
with
    description "Un entrebâilleur de sécurité à chaîne en acier inoxydable.",
    name 'chaîne' 'entrebâilleur',
    article ARTICLE_UNE,
    before [;
        Lock, Unlock, Take, Remove:
            "Il suffit d'être du bon côté de la porte et d'OUVRIR ou FERMER
            l'entrebâilleur.";
        Open:
            if (player in cuisine) {
                if (self hasnt open) {
                    if (porte_cellier has open)
                        "Vous glissez votre main par l'entrebâillement de la
                        porte sans parvenir à retirer la chaîne.";
                    else if (bague has worn) {
                        give self open;
                        "Vous passez votre main fantôme au travers de la porte
                        et retirez la chaîne de l'entrebâilleur.";
                    }
                    "C'est impossible, car l'entrebâilleur est de l'autre côté
                    de la porte.";
                }
            }
        Close:
            if (self has open)
                "Vous avez certainement mieux à faire maintenant.";
    ],
has female openable ~open;

Object couteau "couteau" Cuisine
with
    description "Un couteau à viande bien affûté.",
    name 'couteau',
    before [;
        Take, Remove, Pull:
            "Le couteau est profondément planté dans une table à découper et
            n'en bougera pas.";
    ],
    article ARTICLE_UN,
has;

Object barre "barre métallique" Cuisine
with
    description "Un pied-de-biche à décoffrer apprécié des cambrioleurs.",
    name 'barre' 'métallique' 'pied-de-biche',
    article ARTICLE_UNE,
has female concealed;

Object pomme "pomme" Cellier
with
    description "Une pomme d'or.", !TODO
    name 'pomme' 'or',
    article ARTICLE_UNE,
has female edible;

Object lustre "lustre d'apparat" Cage_escalier
with
    description "Un lustre jaillissant de l'étage supérieur.",
    name 'lustre',
    article ARTICLE_UN,
has scenery;

Object rampe "rampe d'escalier" Cage_escalier
with
    description "Une rampe en fer forgé ornée de boules de départ en forme
    de crâne.",
    name 'rampe',
    article ARTICLE_UNE,
has female scenery;

Object crane "crâne" Cage_escalier
with
    description "Un crâne décoratif creux en marbre blanc.",
    name 'crâne',
    article ARTICLE_UN,
    react_before [;
        if (action == ##Insert && second == self)
          <<Fill (second) (noun)>>;
    ],
    before [;
        Fill:
            if (second ~= vin or eau)
                "Vous ne pouvez pas remplir le crâne avec cela.";
            if (eau in self)
                "Le crâne contient déjà de l'eau.";
            if (second == vin) {
                remove vin;
                move maillet to tonneau;
                "Le crâne se gorge jusqu'à la lie de cette divine libation,
                révélant un maillet.";
            }
            move eau to crane;
            "Le crâne s'abreuve et se remplit de ce goutte-à-goutte
            providentiel.";
        Empty:
            if (eau in self) { !TODO vin
                remove eau;
                "Le crâne restitue cette manne aux formes de vie futur.";
            }
        Wear:
            "Vous ne croyez pas en la vie après la mort.";
    ],
has concealed container open clothing ~worn;

Object pendule "pendule" Salon
with
    description "Une pendule de cheminée arrêtée, qui donne l'heure exacte deux
    fois par jour.",
    name 'pendule',
    article ARTICLE_UNE,
has female scenery; !switchable

Object fauteuil "fauteuil" Salon
with
    description [;
         if (self has general)
            "Un fauteuil en cuir disposé prés de la cheminée.";
        "Un fauteuil en cuir, curieusement accolé au mur du salon.";
        !pour gagner de la place / l'angle du salon
    ],
    name 'fauteuil',
    article ARTICLE_UN,
    before [;
        Enter:
            if (cle hasnt moved) {
                if (self has general) {
                    move_to_player(cle);
                    print "Vous vous abandonnez avec délice dans le fauteuil
                    et ne tardez pas à vous assoupir...";
                    anyKey();
                    print "^^Vous vous réveillez en sursaut, la main crispée
                    sur un objet qu'un visiteur vous a remis pendant votre
                    sommeil.^^";
                    give chambre general;
                    PlayerTo(Chambre);
                    rtrue;
                }
                "Le fauteuil est confortable, mais l'endroit peu propice à la
                rêverie.^^Vous vous relevez.";
            }
            "", (string) MSG_SLEEP_DEFAULT, "";
        Push, Pull:
            if (self hasnt general) {
                give self general;
                print "Vous déplacez le fauteuil vers l'âtre de la cheminée";
                if (livret hasnt moved) {
                    move livret to Salon;
                    print ", révélant un livret d'opéra";
                }
                ".";
            }
            if (livret hasnt moved)
                remove livret;
            give self ~general;
            "Vous remettez le fauteuil à sa place initiale.";
    ],
    after [;
        Enter:
            "TODO";
    ]
has scenery ~general enterable;

Object tison "tison" Salon
with
    description "Un morceau de bois en partie brûlé et encore en ignition.",
    name 'tison',
    article ARTICLE_UN,
    before [;
        Take, Remove:
            "Vous préférez lâcher prise afin d'éviter toute combustion
            spontanée.";
        SwitchOff:
            !"Un agent extincteur, le plus classique qui soit, devrait permettre
            !d'étouffer l’affaire.";
    ],
has;

Object lit "lit" Chambre
with
    description "Un lit défait aux draps froissés et souillés.",
    name 'lit',
    article ARTICLE_UN,
    before [;
        Enter:
            if (verb_word == 'asseoir')
                "", (string) MSG_SLEEP_DEFAULT, "";
            "Vous ne dormez que dans des draps propres et sans plis.";
    ],
has scenery enterable supporter;

Object armoire "armoire" Chambre
with
    description [;
        print "Une armoire de chambre à coucher ";
        if (self has open)
            "avec penderie et plusieurs tiroirs.";
        "placée à côté du lit.";
    ],
    name 'armoire',
    article ARTICLE_UNE_,
has female scenery container openable ~open;

Object robe "robe" armoire
with
    short_name [;
        if (self in player) {
            print "robe élégante";
            rtrue;
        }
    ],
    description "Une robe longue vibrante et lumineuse à décolleté plongeant
    avec nœud et fente aux cuisses.",
    name 'robe',
    article ARTICLE_UNE,
    before [;
        Disrobe, Drop, Give:
            "Vous seriez nue et sans poches.";
    ],
    after [;
        Take, Remove:
            give self worn;
            move peignoir to armoire;
            "Vous retirez votre peignoir et enfilez la robe par le bas pour la
            remonter le long de votre corps."; !TODO
    ]
has female clothing ~worn;

Object tiroir_1 "1er tiroir" armoire
with
    description "Un tiroir avec un bouton de meuble nacarat.",
    name '1er' 'tiroir' 'bouton' 'nacarat',
    article ARTICLE_UN,
    capacity 1
has static container openable ~open ~locked;

Object tiroir_2 "2e tiroir" armoire
with
    description "Un tiroir avec un bouton de meuble safre.",
    name '2e' 'tiroir' 'bouton' 'safre',
    article ARTICLE_UN,
    before [;
        Open:
            if (self hasnt open && (tiroir_1 has open || tiroir_3 has open))
                "Le tiroir récalcitrant refuse de s'ouvrir.";
            if (bague in self) {
                give tiroir_1 locked;
                give tiroir_3 locked;
            }
        Close:
            if (self has open && (tiroir_1 hasnt open || tiroir_3 hasnt open))
                "Le tiroir récalcitrant refuse de se refermer.";
    ],
    after [;
        Close:
            if (bague hasnt moved) {
                if (child(self))
                    move (child(self)) to coffre;
                move bague to self;
            }
    ],
    capacity 1
has static container openable open;

Object tiroir_3 "3e tiroir" armoire
with
    description "Un tiroir avec un bouton de meuble malachite.",
    name '3e' 'tiroir' 'bouton' 'malachite',
    article ARTICLE_UN,
    capacity 1
has static container openable ~open ~locked;

Object coffre "coffre" Chambre
with
    description "Un coffre destiné aux objets perdus puis retrouvés.",
    parse_name [;
        if (NextWord() ~= 'coffre')
            return 0;
        if (NextWord() == 'à//' && NextWord() == 'jouets')
            return 3;
        return 1;
    ],
    article ARTICLE_UN,
    !before [;
    !    LetGo: "";
    !],
    !after [;
    !    Receive:
    !],
    !capacity 100,
has scenery container openable open; ! enterable;

Object tonneau "tonneau" Cave
with
    description "Un fût de 50 litres en bois de chêne.",
    name 'tonneau',
    article ARTICLE_UN,
    before [;
        Open:
            if (self hasnt open)
                "Le tonneau est fermé par une pièce de fond, à faire sauter
                incontinent.";
        Empty:
            "Le fût est dépourvu de trou de bonde.";
        Unlock:
            if (second ~= barre)
                "L'idéal serait un tire-botte ou un chausse-pied mais le jeu en
                est dépourvu.";
            if (self hasnt locked) {
                PrintMsg(MSG_OPEN_ALREADY);
                rtrue;
            }
        Receive: !TODO
            !if (vin in self)
        !LetGo:
    ],
    after [;
        Unlock:
            give self open;
            move vin to self;
            "Vous faites sauter la maîtresse pièce d'un coup de pied-de-biche,
            révélant du vin.";
        Enter:
            !TODO si vin ou maillet ou vide
    ],
    with_key barre,
    !capacity 1
has scenery container openable ~open lockable locked enterable;

Object fenetre "fenêtre à lancette" Palier
with
    description "Une fenêtre à point de vue subjectif.",
    name 'fenêtre',
    article ARTICLE_UNE,
has female scenery;

Object plante "plante" Palier
with
    description [;
         if (self has general)
            "TODO";
        "Une plante d'intérieur à base de résine de polyester.";
    ],
    name 'plante',
    article ARTICLE_UNE,
has female scenery ~general; !edible

Object vitrail "vitrail" Chapelle
with
    description "TODO",
    name 'vitrail',
    article ARTICLE_UN,
    before [;
        !Attack:
        ThrownAt:
            if (noun == pomme) {
                move cercueil to Chapelle;
                remove pomme; ! dans coffre ?
                give self ~concealed general;
                print "Le vitrail se brise TODO^";
                if (cierge has on)
                    <<SwitchOff (cierge)>>;
                rtrue;
            }
    ],
has scenery ~general;

Object cierge "cierge" Chapelle
with
    description "Un lourd et grand cierge gradué en attente d'être consumé.",
    name 'cierge',
    article ARTICLE_UN,
    before [;
        SwitchOn, Burn:
            if (self hasnt on && brandon notin player)
                "Vous n'avez rien sur vous pour ranimer sa flamme.";
    ],
    after [;
        SwitchOn:
            move cercueil to Chapelle;
        SwitchOff:
            if (vitrail hasnt general)
                remove cercueil;
            "Le cierge se meurt exhalant un panache de fumée blanche et âcre.";
    ]
has scenery switchable ~on; !light

Object cercueil "cercueil" Chapelle
with
    description "Un cercueil au bois sombre et corrompu posé sur un autel de
    jaspe rouge.",
    name 'cercueil',
    article ARTICLE_UN,
    !capacity
has scenery container openable ~open;

Object vampire "vampire" cercueil
with
    description "TODO",
    name 'vampire',
    article ARTICLE_UN,
has animate;
